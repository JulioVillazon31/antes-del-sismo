//
//  Instrucciones_ViewController.swift
//  Antes del Sismo
//
//  Created by Julio Villazon on 10/22/17.
//  Copyright © 2017 Julio Villazon. All rights reserved.
//

import UIKit

class Instrucciones_ViewController: UIViewController {

    var antesFinal = String()
    var duranteFinal = String()
    var despuesFinal = String()
    
    var page = 0
    
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var lblState: UILabel!
    
    
    
    @IBAction func `continue`(_ sender: Any)
    {
        if(page == 0)
        {
            textView.text = duranteFinal
            lblState.text = "Durante..."
        }
        
        if(page == 1)
        {
            textView.text = despuesFinal
            lblState.text = "Despues..."
        }
        
        if(page < 2)
        {
            page += 1
        }
        
        if(page == 2)
        {
            btnContinue.isEnabled = false
        }
        
    }

    @IBAction func back(_ sender: Any) {
        
        if(page == 2)
        {
            btnContinue.isEnabled = true
            textView.text = duranteFinal
            lblState.text = "Durante..."
        }
        
        if(page == 1)
        {
            textView.text = antesFinal
            lblState.text = "Antes..."
        }
        
        if(page == 0)
        {
            performSegue(withIdentifier: "goBack", sender: self)
            page += 1
        }
        
        page -= 1
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.text = antesFinal
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  Cuentanos_ViewController.swift
//  Antes del Sismo
//
//  Created by Julio Villazon on 10/22/17.
//  Copyright © 2017 Julio Villazon. All rights reserved.
//

import UIKit

class Cuentanos_ViewController: UIViewController {

    var antesText = String()
    var duranteText = String()
    var despuesText = String()
    
    @IBOutlet weak var empleado: UISegmentedControl!
    @IBOutlet weak var estudiante: UISegmentedControl!
    @IBOutlet weak var casado: UISegmentedControl!
    
    @IBOutlet weak var hijos: UISegmentedControl!
    @IBOutlet weak var mascotas: UISegmentedControl!
    @IBOutlet weak var servidumbre: UISegmentedControl!
    @IBOutlet weak var abuelos: UISegmentedControl!
    
    @IBOutlet weak var costa: UISegmentedControl!
    @IBOutlet weak var departamento: UISegmentedControl!
    @IBOutlet weak var piso: UISegmentedControl!
    
   
    @IBAction func depaListener(_ sender: Any)
    {
        if(departamento.selectedSegmentIndex == 1)
        {
            piso.isEnabled = false
        }
        else
        {
            piso.isEnabled = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        piso.isEnabled = false
    }
    
    @IBAction func continuar(_ sender: Any)
    {
        antes()
        durante()
        despues()
    }
    
    func antes()
    {
        antesText = "► Dejar a la mano cerca de la salida: Actas de nacimiento, Escrituras de inmuebles"
        
        if(hijos.selectedSegmentIndex == 0)
        {
            antesText += ", Actas de hijos"
        }
        
        if(casado.selectedSegmentIndex == 0)
        {
            antesText += ", Actas de matrimonio"
        }
        
        antesText += ".\n\n"
        
        if(servidumbre.selectedSegmentIndex == 0)
        {
            antesText += "► Debe de conocer los procedimientos (sino, mencionárselos) y deben de salir por su propio medio.\n\n"
        }
        
        antesText += "► Conocer un plan de emergencia local, identificar salidas de emergencia y puntos de reunión.\n\n"
        
        antesText += "► En México no es recomendable resguardarse debajo del marco de una puerta o utilizar el “Triangulo de seguridad” durante un derrumbe ya que fue diseñado para materiales livianos como los que se usan en EUA.\n\n► Identificar zonas de seguridad por medio de un ingeniero o arquitecto que evalúen la estructura para identificar las zonas mas rígidas y que resisten mas carga. \n\n►Alejarse de cristales u objetos que se puedan caer."
    }
    
    func durante()
    {
        duranteText = "► No corras!\n\n"
        
        if(hijos.selectedSegmentIndex == 0)
        {
            duranteText += "► Si tu hijo no tiene capacidad de caminar debes de cargarlo e irte con él. Si tiene la capacidad de caminar debes tratar de ir con él a alguna zona de seguridad.\n\n"
        }
        
        if(mascotas.selectedSegmentIndex == 0)
        {
            duranteText += "► Si tu mascota es pequeña y puedes cargarla, tomala y llevala contigo, si no, sal y el/ella te seguiran.\n\n"
        }
        
        if(departamento.selectedSegmentIndex == 0)
        {
            if(piso.selectedSegmentIndex == 0 || piso.selectedSegmentIndex == 1 || piso.selectedSegmentIndex == 2||piso.selectedSegmentIndex == 3)
            {
                duranteText += "► Baja de tu edificio por las escaleras, nunca uses el elevador ya que se puede bloquear y tu quedar atorado.\n\n"
            }
            else
            {
                duranteText += "► No uses las escaleras.\n\n"
                duranteText += "► No salgas del edificio y no subas a la azotea . Buscar muros de carga o castillos para el sobre esfuerzo que hace el edificio.\n\n"
            }
        }
        else
        {
            duranteText += "► Sal de tu casa dejando toda pertenencia atras.\n\n"
        }
        
        if(estudiante.selectedSegmentIndex == 0)
        {
            duranteText += "► En caso de estar en clase, interrumpir inmediatamente y buscar alguna de las zonas de seguridad marcadas previamente por protección civil del colegio.\n\n"
        }
        
        if(costa.selectedSegmentIndex == 0)
        {
            duranteText += "► Estar alerta de indicaciones de protección civil por si hubo regreso de agua y hay peligro de tsunami.\n\n"
        }
        
        duranteText += "► Si salio a la calle, alejese de cables, postes o cualquier cosa que pueda caerse."
    }
    
    func despues()
    {
        despuesText = "► En caso de haber quedado dentro de una estructura, hay que evaluarla, para determinar si hay algún daño en la estructura y ver si puedes salir o no.\n\n► Si saliste del inmueble, no subas otra vez hasta que se haya hecho una evaluación del edificio.\n\n► En caso de que huela a gas, trata de cortar los suministros principales , como lo son los tanques portátiles o centrales.\n\n► Si tu estas bien y quieres ayudar, en primera instancia no ingresar al edificio para no tener riesgo. Hacer un conteo para ver quién falta. Si alguien falta, dar su posible ubicación a grupos de rescate. Asimismo, poder traer agua y comida.\n\n► Después del sismo puedes acudir a protección civil para saber si tu casa todavía es funcional en base a un ingeniero."
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let instructionsController = segue.destination as! Instrucciones_ViewController
        instructionsController.antesFinal = antesText
        instructionsController.duranteFinal = duranteText
        instructionsController.despuesFinal = despuesText
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
